# Clean Cursor Stream Library

The Clean Cursor Stream library provides a wrapper for `FUNDAMENTAL-CHARACTER-INPUT-STREAM` objects that reports the file position in the form of line and column numbers.
It is intended to be used by programs that ingest text files to report the locations of problems that occur.

## Clean Project

This library is part of the [Clean Project](https://gitlab.com/clean),
the outlet for my NIH syndrome.

## License

This library was written by Philipp Matthias Schäfer
(philipp.matthias.schaefer@posteo.de) and is published under the GPL3 license.
See [LICENSE] for a copy of that license.
