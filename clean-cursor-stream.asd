;;;; Copyright (c) 2023, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
;;;; 
;;;; This file is part of the Clean Cursor Stream library.
;;;; 
;;;; The Clean Cursor Stream library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;; 
;;;; The Clean Cursor Stream library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Cursor Stream library. If not, see <https://www.gnu.org/licenses/>.

(in-package :asdf-user)

#-asdf3.1 (error "Clean Cursor Stream requires ASDF 3.1 or later. Please upgrade your ASDF.")

(defsystem :clean-cursor-stream
  :description "TODO"
  :long-description #.(read-file-string "README.md")
  :author "Philipp Matthias Schäfer <philipp.matthias.schaefer@posteo.de>"
  :licence "GNU General Public License Version 3"
  :version "1.0.0"
  :class :package-inferred-system
  :depends-on (:clean-cursor-stream/source/all)
  :in-order-to ((test-op (load-op :clean-cursor-stream/tests/all
                                  :clean-standard-out-test-reporter)))
  :perform (test-op (o c) (symbol-call :clean-test :run-all)))

;;; Systems
(register-system-packages :clean-cursor-stream/source/all '(:clean-cursor-stream/source/all))

;;; Additional Systems for Tests
(register-system-packages :clean-cursor-stream/tests/all '(:clean-cursor-stream/tests/all))
