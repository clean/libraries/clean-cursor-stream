;;;; Copyright (c) 2023, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Cursor Stream library.
;;;;
;;;; The Clean Cursor Stream library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Cursor Stream library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Cursor Stream library. If not, see <https://www.gnu.org/licenses/>.

(defpackage :clean-cursor-stream/tests/all
  (:use :common-lisp
        :clean-cursor-stream
        :clean-test
        :clean-util))

(in-package :clean-cursor-stream/tests/all)

(defmacro %define-base-test-case ((name stream string) &body body)
  (with-gensyms (inner-stream)
    `(define-test-case (,name)
       (with-input-from-string (,inner-stream ,string)
         (let ((,stream (make-instance 'cursor-character-input-stream :stream ,inner-stream)))
           ,@body)))))

(defun check-cursor (line column cursor)
  (assert-that (= line (cursor-line cursor)))
  (assert-that (= column (cursor-column cursor))))

(defmacro define-error-test-case ((name line column stream string) &body body)
  `(%define-base-test-case (,name ,stream ,string)
     ,@body
     (handler-case (located-error ,stream 'located-stream-error)
       (located-stream-error (condition)
         (check-cursor ,line ,column (get-cursor condition))))))

(defmacro define-cursor-test-case ((name line column stream string) &body body)
  `(%define-base-test-case (,name ,stream ,string)
     ,@body
     (check-cursor ,line ,column (get-cursor ,stream))))

(with-test-suite (clean-cursor-stream)

  (define-cursor-test-case (initial-value 1 0 stream "String"))

  (define-cursor-test-case (read-char 1 1 stream "String")
    (read-char stream))

  (define-cursor-test-case (single-eof 1 6 stream "String")
    (dotimes (_ 6)
      (read-char stream))

    (assert-that (eq :eof (read-char stream nil :eof))))

  (define-cursor-test-case (multiple-eof 1 6 stream "String")
    (dotimes (_ 8)
      (read-char stream nil :eof)))

  (define-cursor-test-case (unread-char 1 0 stream "String")
    (read-char stream)
    (unread-char #\S stream))

  (define-cursor-test-case (peek-char 1 0 stream "String")
    (peek-char nil stream))

  (define-cursor-test-case (newline-read-char 2 0 stream "String
")
    (dotimes (_ 7)
      (read-char stream)))

  (define-cursor-test-case (newline-read-line 2 0 stream "String
")
    (read-line stream))

  (define-cursor-test-case (single-line-seek-forward 1 3 stream "String")
    (file-position stream 3))

  (define-cursor-test-case (single-line-seek-backward 1 3 stream "String")
    (dotimes (_ 6)
      (read-char stream))
    (file-position stream 3))

  (define-cursor-test-case (multi-line-seek-forward 2 3 stream "String
String
String")
    (file-position stream 10))

  (define-cursor-test-case (multi-line-seek-backward 2 3 stream "String
String
String")
    (read-line stream)
    (read-line stream)
    (read-line stream)
    (file-position stream 10))

  (define-error-test-case (located-error 1 4 stream "String")
    (dotimes (_ 4)
      (read-char stream))))
