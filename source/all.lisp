;;;; Copyright (c) 2023, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Cursor Stream library.
;;;;
;;;; The Clean Cursor Stream library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Cursor Stream library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Cursor Stream library. If not, see <https://www.gnu.org/licenses/>.

(uiop:define-package :clean-cursor-stream/source/all
  (:nicknames :clean-cursor-stream)
  (:use :clean-util
        :common-lisp
        :sb-gray)
  (:export :cursor
           :cursor-character-input-stream
           :cursor-column
           :cursor-line
           :get-cursor
           :located-error
           :located-stream-error

           :*tab-width*))

(in-package :clean-cursor-stream/source/all)

(defclass cursor ()
  ((line :initform 1
         :reader cursor-line)
   (column :initform 0
           :reader cursor-column)
   (line-widths :initform nil))
  (:documentation
   "A CURSOR object represents a position, line and column, within a FUNDAMENTAL-CHARACTER-INPUT-STREAM.

Line numbers start counting at 1, while column numbers start counting at 0.
The line number and column number can be read from a CURSOR object using the methods CURSOR-LINE and CURSOR-COLUMN"))

(defgeneric cursor-line (cursor)
  (:documentation "Returns the line number of a CURSOR object.

Line numbers start counting at 1."))

(defgeneric cursor-colum (cursor)
  (:documentation "Returns the column number of a CURSOR object.

Column numbers start counting at 1."))

(define-condition located-stream-error (stream-error)
  ((cursor :initarg :cursor
           :reader get-cursor))
  (:documentation
   "A base condition for errors that occur during processing of a CURSOR-CHARACTER-INPUT-STREAM.

It holds a cursor object, representing the current line and column position.
The cursor can be read from the condition using the method GET-CURSOR."))

(defun located-error (stream type &rest arguments)
  "Invoke ERROR on LOCATED-STREAM-ERROR type condition initialized with CURSOR-CHARACTER-INPUT-STREAM STREAM, its cursor and ARGUMENTS."
  (check-type stream cursor-character-input-stream)
  (assert (subtypep 'located-stream-error type))
  (apply #'error type (list* :cursor (get-cursor stream) :stream stream arguments)))

(defclass cursor-character-input-stream (fundamental-character-input-stream)
  ((stream :initarg :stream
           :initform (missing-initarg 'stream 'cursor-character-input-stream))
   (cursor :initform (make-instance 'cursor)))
  (:documentation
   "A CURSOR-CHARACTER-INPUT-STREAM is a FUNDAMENTAL-CHARACTER-INPUT-STREAM that wraps another FUNDAMENTAL-CHARACTER-INPUT-STREAM that tracks line and column position.

The cursor, representing the current line and column position, can be read from a CURSOR-CHARACTER-INPUT-STREAM using the function GET-CURSOR."))

(defun %copy-of-cursor (cursor &optional with-line-widths-p)
  (let-return (copy-of-cursor (make-instance 'cursor))
    (with-slots (line column line-widths) copy-of-cursor
      (setf line (cursor-line cursor)
            column (cursor-column cursor))
      (when with-line-widths-p
        (setf line-widths (copy-list (slot-value cursor 'line-widths)))))))

(defmethod get-cursor ((stream cursor-character-input-stream))
  "Returns a copy of the CURSOR of STREAM, a CURSOR-CHARACTER-INPUT-STREAM."
  (with-slots (cursor) stream
    (%copy-of-cursor cursor)))

(defparameter *tab-width* 2
  "Column width of a tab character for CURSOR-CHARACTER-INPUT-STREAM.")

(defmethod close ((c-stream cursor-character-input-stream) &key abort)
  (with-slots (stream) c-stream
    (close stream :abort abort)))

(defmethod stream-read-char ((c-stream cursor-character-input-stream))
  (with-slots (stream cursor) c-stream
    (with-slots (line column line-widths) cursor
      (let-return (char (read-char stream nil :eof))
        (case char
          (#\Newline
           (incf line)
           (push column line-widths)
           (setf column 0))
          (#\Tab (incf column *tab-width*))
          (:eof)
          (otherwise (incf column)))))))

(defmethod stream-unread-char ((c-stream cursor-character-input-stream)
                               character)
  (with-slots (stream cursor) c-stream
    (with-slots (line column line-widths) cursor
      (case character
        (#\Newline
         (decf line)
         (setf column (pop line-widths)))
        (#\Tab (decf column *tab-width*))
        (otherwise (decf column)))
      (unread-char character stream))))

(defun %move-forward-to-pos (c-stream position-spec)
  (with-slots (stream cursor) c-stream
    (let ((start-cursor (%copy-of-cursor cursor t))
          (start-position (file-position stream)))
      (while (< (file-position stream) position-spec)
        (let ((char (read-char c-stream nil :eof)))
          (when (and (eq char :eof)
                     (< (file-position stream) position-spec))
            (setf cursor start-cursor)
            (file-position stream start-position)
            (return-from %move-forward-to-pos nil))))
      (file-position stream position-spec)
      t)))

(defun %move-backward-to-position (c-stream position-spec)
  (with-slots (stream cursor) c-stream
    (with-slots (line column line-widths) cursor
      (let ((position (file-position stream)))
        ;; Push column as line, because the loop is written with the assumption
        ;; that we are at the start of a line and know we are to far.
        (push column line-widths)
        (incf position)
        (incf line)
        (while (> position position-spec)
          (decf position)
          (decf line)
          (setf column (pop line-widths))
          (when (not (= position position-spec))
            (if (< (- position column) position-spec)
                (progn
                  (decf column (- position position-spec))
                  (setf position position-spec))
                (decf position column)))
          (file-position stream position-spec))))))

(defmethod stream-file-position ((c-stream cursor-character-input-stream)
                                 &optional position-spec)
  (with-slots (stream cursor) c-stream
    (if position-spec
        (let ((position (file-position stream)))
          (cond
            ((< position-spec 0) nil)
            ((= position position-spec) t)
            ((< position position-spec)
             (%move-forward-to-pos c-stream position-spec))
            (t (%move-backward-to-position c-stream position-spec))))
        (file-position stream))))
